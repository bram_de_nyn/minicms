@extends('app')

@section('content')

    <div class="small_container">

        <h1>Add some content</h1>

        <form action="/register" class="flex_form" method="POST">
            {!! csrf_field() !!}

            <div class="flex_container">
                <label for="url" class="form_label">URL</label>
                <input type="url" name="url">
            </div>

            <div class="flex_container cta_container">
                <button class="cta_btn" type="submit"><span>Add</span></button>
            </div>

        </form>

    </div>

@stop