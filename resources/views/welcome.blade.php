@extends('app')

@section('content')

    <div class="small_container">

        <h1>Welcome to Anhinga's MiniCMS!</h1>

        <p class="thin center">Do you want to log in? or create an account?</p>

        <div class="cta_container">

            <a href="/login" class="cta_btn">Login</a>
            <a href="/register" class="cta_btn">Register</a>

        </div>

    </div>

@stop
