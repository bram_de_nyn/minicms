@extends('app')

@section('content')

    <div class="small_container">

        <h1>This is the index page</h1>

        <div class="cta_container">
            <a href="/content/add" class="cta_btn">Add Content</a>
        </div>

    </div>

@stop