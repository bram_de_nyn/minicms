@extends('app')

@section('content')

    <div class="small_container">

        <h1>Log in to the miniCMS</h1>

        <form action="/register" class="flex_form" method="POST">
            {!! csrf_field() !!}

            <div class="flex_container">
                <label for="email" class="form_label">Email</label>
                <input type="email" name="email">
            </div>

            <div class="flex_container">
                <label for="password" class="form_label">Password</label>
                <input type="password" name="password">
            </div>

            <div class="flex_container cta_container">
                <button class="cta_btn" type="submit"><span>Login</span></button>
            </div>

        </form>

    </div>

@stop