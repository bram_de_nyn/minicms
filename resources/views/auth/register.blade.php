@extends('app')

@section('content')

    <div class="small_container">

        <h1>Register for the miniCMS</h1>

        <form action="/register" class="flex_form" method="POST">
            {!! csrf_field() !!}

            <div class="flex_container">
                <label for="name" class="form_label">Username</label>
                <input type="name" name="name">
            </div>

            <div class="flex_container">
                <label for="email" class="form_label">Email</label>
                <input type="email" name="email">
            </div>

            <div class="flex_container">
                <label for="password" class="form_label">Password</label>
                <input type="password" name="password">
            </div>

            <div class="flex_container">
                <label for="password_confirmation" class="form_label">Confirm password</label>
                <input type="password" name="password_confirmation">
            </div>

            <div class="flex_container">
                <label for="code" class="form_label">Secret code</label>
                <input type="code" name="code">
            </div>

            <div class="flex_container cta_container">
                <button class="cta_btn" type="submit"><span>Register</span></button>
            </div>

        </form>

    </div>

@stop