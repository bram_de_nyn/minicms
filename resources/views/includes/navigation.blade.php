<nav>
    <a href="/">Home</a>


    @if (Auth::check())
    {
        <a href="/logout" class="logout_link">Logout</a>
    }
    @endif

</nav>

<div class="cf"></div>