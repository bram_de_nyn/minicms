<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation messages that apply to the rules.
     *
     *
     */
    public function messages()
    {
        return [

            'email.required' => "You forgot to give your email" ,
            'email.email' => "Please give a valid email address" ,

            'password.required' => "Please select an image for your project." ,

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
        ];
    }
}
