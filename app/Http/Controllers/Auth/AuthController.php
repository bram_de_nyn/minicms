<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

////////// DEFINING USED FUNCTIONS AND VARIABLES //////////

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

////////// DEFINING USED FUNCTIONS AND VARIABLES //////////



////////// GET REGISTER //////////

    public function getRegister()
    {

        return view('auth.register');
    }
////////// GET REGISTER //////////



////////// POST REGISTER //////////

    public function postRegister(RegisterUserRequest $request)
    {

        $messages = [
            'name.required' => "You forgot to give your name" ,
            'name.max' => "Please give a shorter name" ,

            'email.required' => "You forgot to give your email" ,
            'email.email' => "Please give a valid email address" ,
            'email.max' => "Pleas give a shorter email address" ,
            'email.unique' => "This email address already exists" ,

            'password.required' => "Please select an image for your project." ,
            'password.confirmed' => "Passwords do not match" ,
            'password.min' => "Password has to be at least 6 characters" ,

            'code.required' => "You need the secret code to log in" ,
        ];

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'code' => 'required'
        ], $messages);


        $code = $request->get("code");

        if($code == "9092410190783832")
        {

            Auth::login($this->create($request->all()));

            return redirect('/')->with('message', "Your account has been created, and you are now logged in");
        }
        else
        {
            return Redirect::back()->withErrors($request)->withInput();
        }
    }

////////// POST REGISTER //////////


////////// GET LOGIN //////////

    public function getLogin()
    {
        return view("auth.login");
    }

////////// GET LOGIN //////////



////////// POST LOGIN //////////


    public function postLogin(Request $request)
    {

        $messages = [
            'email.required' => "Please give your email address" ,
            'email.email' => "Please give a valid email address" ,

            'password.required' => "Please select an image for your project." ,
        ];

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ], $messages);


        $email = $request->get("email");
        $password = $request->get("password");

        if (Auth::attempt(['email' => $email, 'password' => $password])) {

            return redirect()->intended('/');

        }

        return Redirect::back()->withErrors($request)->withInput();
    }

////////// POST LOGIN //////////

}
