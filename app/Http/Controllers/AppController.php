<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;


class AppController extends Controller
{

///////////////////////////////////////////////////////////

    public function index()
    {
        if (!Auth::check())
        {
            return view("welcome");
        }

        return view("index");

    }

///////////////////////////////////////////////////////////

    public function welcome()
    {
        if (!Auth::check())
        {
            return view("welcome");
        }

        return view('index');
    }

///////////////////////////////////////////////////////////


    public function getContentAdd()
    {
        if (!Auth::check())
        {
            // return view("welcome");
            return view("addContent");
        }

        return view("addContent");
    }

///////////////////////////////////////////////////////////

}
